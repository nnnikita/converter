public class MetodCalc
{
    public double calc (double num, String op)
    {
        double res = 0;
        switch (op)
        {

//            TIME

            case "SecToMin":
                res = num / 60;
                break;
            case "SecToHour":
                res = num / 3600;
                break;
            case "SecToDay":
                res = num / 86400;
                break;
            case "SecToWeek":
                res = num / 604800;
                break;
            case "SecToMonth":
                res = num / 2592000;
                break;
            case "SecToYear":
                res = num / 31536000;
                break;
            case "MinToSec":
                res = num * 60;
                break;
            case "HourToSec":
                res = num * 3600;
                break;
            case "DayToSec":
                res = num * 86400;
                break;
            case "WeekToSec":
                res = num * 604800;
                break;
            case "MonthToSec":
                res = num * 2592000;
                break;
            case "YearToSec":
                res = num * 31536000;
                break;

//            WEIGHT

            case "KgToGram":
                res = num * 1000;
                break;
            case "KgToCarat":
                res = num * 5000;
                break;
            case "KgToEngPound":
                res = num / 0.45359237;
                break;
            case "KgToPound":
                res = num / 0.3732417216;
                break;
            case "KgToStone":
                res = num / 0.15747304441777;
                break;
            case "KgToRusPound":
                res = num / 0.4095124;
                break;
            case "GramToKg":
                res = num / 1000;
                break;
            case "CaratToKg":
                res = num / 5000;
                break;
            case "EngPoundToKg":
                res = num * 0.45359237;
                break;
            case "PoundToKg":
                res = num * 0.3732417216;
                break;
            case "StoneToKg":
                res = num * 0.15747304441777;
                break;
            case "RusPoundToKg":
                res = num * 0.40951240;
                break;

//                VOLUME

            case "LtoM3":
                res = num / 1000;
                break;
            case "LtoGal":
                res = num / 3.785;
                break;
            case "LtoPint":
                res = num / 0.56826125;
                break;
            case "LtoQuart":
                res = num / 1.1365;
                break;
            case "LtoBarrel":
                res = num / 163.66;
                break;
            case "LtoCubicFoot":
                res = num / 28.3169;
                break;
            case "LtoCubicInch":
                res = num / 0.016387064;
                break;

            case "M3ToL":
                res = num * 1000;
                break;
            case "GalToL":
                res = num * 3.785;
                break;
            case "PintToL":
                res = num * 0.56826125;
                break;
            case "QuartToL":
                res = num * 1.1365;
                break;
            case "BarrelToL":
                res = num * 163.66;
                break;
            case "CubicFootToL":
                res = num * 28.3169;
                break;
            case "CubicInchToL":
                res = num * 0.016387064;
                break;

//            Length

            case "MetreToKilometre":
                res = num / 1000;
                break;
            case "MetreToMile":
                res = num / 1609.34;
                break;
            case "MetreToNauMile":
                res = num / 1852;
                break;
            case "MetreToCable":
                res = num / 185.2;
                break;
            case "MetreToLeague":
                res = num / 4827;
                break;
            case "MetreToFoot":
                res = num / 0.3048;
                break;
            case "MetreToYard":
                res = num / 0.9144;
                break;

            case "KilometreToMetre":
                res = num * 1000;
                break;
            case "MileToMetre":
                res = num * 1609.34;
                break;
            case "NauMileToMetre":
                res = num * 1852;
                break;
            case "CableToMetre":
                res = num * 185.2;
                break;
            case "LeagueToMetre":
                res = num * 4827;
                break;
            case "FootToMetre":
                res = num * 0.3048;
                break;
            case "YardToMetre":
                res = num * 0.9144;
                break;

//            Temperature

            case "CeToKe":
                res = num + 273.15;
                break;
            case "CetoFa":
                res = num * 1.8 + 32;
                break;
            case "CeToRe":
                res = num * 0.8;
                break;
            case "CeToRo":
                res = num * 21 / 40 + 7.5;
                break;
            case "CeToRa":
                res = (num + 273.15) * 9 / 5;
                break;
            case "CeToNy":
                res = num * 33 / 100;
                break;
            case "CeToDe":
                res = (100 - num) * 3 / 2;
                break;

            case "KeToCe":
                res = num - 273.15;
                break;
            case "FaToCe":
                res = (num - 32) / 1.8;
                break;
            case "ReToCe":
                res = num / 0.8;
                break;
            case "RoToCe":
                res = (num - 7.5) * 40 / 21;
                break;
            case "RaToCe":
                res = (num - 491.67) * 5 / 9;
                break;
            case "NyToCe":
                res = num * 100 / 33;
                break;
            case "DeToCe":
                res = 100 - num * 2 / 3;
                break;

            default:
                res = 0;
                break;
        }
        return res;
    }
}