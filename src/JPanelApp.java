import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class JPanelApp extends JPanel
{
    JTextField txt1 = null;
    JTextField txt2 = null;
    double res = 0;
    String op = "";

    public JPanelApp()
    {
        try
        {
            setLayout(null);

//            TIME

            final JLabel time = new JLabel("Time Converter");
            time.setBounds(90,0,100,20);

            final TextField txt1 = new TextField();
            txt1.setBounds(10, 20, 260, 25);

            Font font = new Font("serif", Font.BOLD, 12);

            JButton bSecToMin = new JButton("Second to Minute");
            bSecToMin.setBounds(10, 60, 130, 20);
            bSecToMin.setFont(font);

            JButton bSecToHour = new JButton("Second to Hour");
            bSecToHour.setBounds(10, 90, 130, 20);
            bSecToHour.setFont(font);

            JButton bSecToDay = new JButton("Second to Day");
            bSecToDay.setBounds(10, 120, 130, 20);
            bSecToDay.setFont(font);

            JButton bSecToWeek = new JButton("Second to Week");
            bSecToWeek.setBounds(10, 150, 130, 20);
            bSecToWeek.setFont(font);

            JButton bSecToMonth = new JButton("Second to Month");
            bSecToMonth.setBounds(10, 180, 130, 20);
            bSecToMonth.setFont(font);

            JButton bSecToYear = new JButton("Second to Year");
            bSecToYear.setBounds(10, 210, 130, 20);
            bSecToYear.setFont(font);

            JButton bMinToSec = new JButton("Minute to Second");
            bMinToSec.setBounds(140, 60, 130, 20);
            bMinToSec.setFont(font);

            JButton bHourToSec = new JButton("Hour to Second");
            bHourToSec.setBounds(140, 90, 130, 20);
            bHourToSec.setFont(font);

            JButton bDayToSec = new JButton("Day to Second");
            bDayToSec.setBounds(140, 120, 130, 20);
            bDayToSec.setFont(font);

            JButton bWeekToSec = new JButton("Week to Second");
            bWeekToSec.setBounds(140, 150, 130, 20);
            bWeekToSec.setFont(font);

            JButton bMonthToSec = new JButton("Month to Second");
            bMonthToSec.setBounds(140, 180, 130, 20);
            bMonthToSec.setFont(font);

            JButton bYearToSec = new JButton("Year to Second");
            bYearToSec.setBounds(140, 210, 130, 20);
            bYearToSec.setFont(font);

            add(txt1);
            add(time);
            add(bSecToMin);
            add(bSecToHour);
            add(bSecToDay);
            add(bSecToWeek);
            add(bSecToMonth);
            add(bSecToYear);
            add(bMinToSec);
            add(bHourToSec);
            add(bDayToSec);
            add(bWeekToSec);
            add(bMonthToSec);
            add(bYearToSec);

            bSecToMin.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToMin";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bSecToHour.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToHour";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bSecToDay.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToDay";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bSecToWeek.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToWeek";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bSecToMonth.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToMonth";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bSecToYear.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "SecToYear";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bMinToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "MinToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bHourToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "HourToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bDayToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "DayToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bWeekToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "WeekToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bMonthToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "bMonthToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

            bYearToSec.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt1.getText());
                    op = "YearToSec";
                    double num = Double.valueOf(txt1.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt1.setText(strRes);
                }
            });

//            WEIGHT

            final JLabel weight = new JLabel("Weight Converter");
            weight.setBounds(380,0,100,20);

            final TextField txt2 = new TextField();
            txt2.setBounds(300, 20, 260, 25);

            JButton bKgToGram = new JButton("Kilo to Gram");
            bKgToGram.setBounds(300, 60, 130, 20);
            bKgToGram.setFont(font);

            JButton bKgToCarat = new JButton("Kilo to Carat");
            bKgToCarat.setBounds(300, 90, 130, 20);
            bKgToCarat.setFont(font);

            JButton bKgToEngPound = new JButton("Kilo to EngPound");
            bKgToEngPound.setBounds(300, 120, 130, 20);
            bKgToEngPound.setFont(font);

            JButton bKgToPound = new JButton("Kilo to Pound");
            bKgToPound.setBounds(300, 150, 130, 20);
            bKgToPound.setFont(font);

            JButton bKgToStone = new JButton("Kilo to Stone");
            bKgToStone.setBounds(300, 180, 130, 20);
            bKgToStone.setFont(font);

            JButton bKgToRusPound = new JButton("Kilo to RusPound");
            bKgToRusPound.setBounds(300, 210, 130, 20);
            bKgToRusPound.setFont(font);

            JButton bGramToKg = new JButton("Gram to Kilo");
            bGramToKg.setBounds(430, 60, 130, 20);
            bGramToKg.setFont(font);

            JButton bCaratToKg = new JButton("Carat to Kilo");
            bCaratToKg.setBounds(430, 90, 130, 20);
            bCaratToKg.setFont(font);

            JButton bEngPoundToKg = new JButton("EngPound to Kilo");
            bEngPoundToKg.setBounds(430, 120, 130, 20);
            bEngPoundToKg.setFont(font);

            JButton bPoundToKg = new JButton("Pound to Kilo");
            bPoundToKg.setBounds(430, 150, 130, 20);
            bPoundToKg.setFont(font);

            JButton bStoneToKg = new JButton("Stone to Kilo");
            bStoneToKg.setBounds(430, 180, 130, 20);
            bStoneToKg.setFont(font);

            JButton bRusPoundToKg = new JButton("RusPound to Kilo");
            bRusPoundToKg.setBounds(430, 210, 130, 20);
            bRusPoundToKg.setFont(font);

            add(txt2);
            add(weight);
            add(bKgToGram);
            add(bKgToCarat);
            add(bKgToEngPound);
            add(bKgToPound);
            add(bKgToStone);
            add(bKgToRusPound);
            add(bGramToKg);
            add(bCaratToKg);
            add(bEngPoundToKg);
            add(bPoundToKg);
            add(bStoneToKg);
            add(bRusPoundToKg);

            bKgToGram.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToGram";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bKgToCarat.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToCarat";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bKgToEngPound.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToEngPound";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bKgToPound.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToPound";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bKgToStone.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToStone";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bKgToRusPound.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "KgToRusPound";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bGramToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "GramToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bCaratToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "CaratToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bEngPoundToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "EngPoundToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bPoundToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "PoundToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bStoneToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "StoneToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });

            bRusPoundToKg.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt2.getText());
                    op = "RusPoundToKg";
                    double num = Double.valueOf(txt2.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt2.setText(strRes);
                }
            });


//                        VOLUME

            final JLabel volume = new JLabel("Volume Converter");
            volume.setBounds(670,0,120,20);

            final TextField txt3 = new TextField();
            txt3.setBounds(590, 20, 260, 25);

            JButton bLtoM3 = new JButton("Lit to Cub Metre");
            bLtoM3.setBounds(590, 60, 130, 20);
            bLtoM3.setFont(font);

            JButton bLtoGal = new JButton("Lit to Gallon");
            bLtoGal.setBounds(590, 90, 130, 20);
            bLtoGal.setFont(font);

            JButton bLtoPint = new JButton("Lit to Pint");
            bLtoPint.setBounds(590, 120, 130, 20);
            bLtoPint.setFont(font);

            JButton bLtoQuart = new JButton("Lit to Quart");
            bLtoQuart.setBounds(590, 150, 130, 20);
            bLtoQuart.setFont(font);

            JButton bLtoBarrel = new JButton("Lit to Barrel");
            bLtoBarrel.setBounds(590, 180, 130, 20);
            bLtoBarrel.setFont(font);

            JButton bLtoCubicFoot = new JButton("Lit to Cubic Foot");
            bLtoCubicFoot.setBounds(590, 210, 130, 20);
            bLtoCubicFoot.setFont(font);

            JButton bLtoCubicInch = new JButton("Lit to Cubic Inch");
            bLtoCubicInch.setBounds(590, 240, 130, 20);
            bLtoCubicInch.setFont(font);

            JButton bM3ToL = new JButton("Cub Metre to Lit");
            bM3ToL.setBounds(720, 60, 130, 20);
            bM3ToL.setFont(font);

            JButton bGalToL = new JButton("Gallon to Lit");
            bGalToL.setBounds(720, 90, 130, 20);
            bGalToL.setFont(font);

            JButton bPintToL = new JButton("Pint to Lit");
            bPintToL.setBounds(720, 120, 130, 20);
            bPintToL.setFont(font);

            JButton bQuartToL = new JButton("Quart to Lit");
            bQuartToL.setBounds(720, 150, 130, 20);
            bQuartToL.setFont(font);

            JButton bBarrelToL = new JButton("Barrel to Lit");
            bBarrelToL.setBounds(720, 180, 130, 20);
            bBarrelToL.setFont(font);

            JButton bCubicFootToL = new JButton("Cubic Foot to Lit");
            bCubicFootToL.setBounds(720, 210, 130, 20);
            bCubicFootToL.setFont(font);

            JButton bCubicInchToL = new JButton("Cubic Inch to Lit");
            bCubicInchToL.setBounds(720, 240, 130, 20);
            bCubicInchToL.setFont(font);

            add(txt3);
            add(volume);
            add(bLtoM3);
            add(bLtoGal);
            add(bLtoPint);
            add(bLtoQuart);
            add(bLtoBarrel);
            add(bLtoCubicFoot);
            add(bLtoCubicInch);
            add(bM3ToL);
            add(bGalToL);
            add(bPintToL);
            add(bQuartToL);
            add(bBarrelToL);
            add(bCubicFootToL);
            add(bCubicInchToL);

            bLtoM3.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoM3";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoGal.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoGal";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoPint.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoPint";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoQuart.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoQuart";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoBarrel.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoBarrel";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoCubicFoot.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoCubicFoot";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bLtoCubicInch.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "LtoCubicInch";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bM3ToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "M3ToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bGalToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "GalToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bPintToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "PintToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bQuartToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "QuartToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bBarrelToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "BarrelToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bCubicFootToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "CubicFootToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

            bCubicInchToL.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt3.getText());
                    op = "CubicInchToL";
                    double num = Double.valueOf(txt3.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt3.setText(strRes);
                }
            });

//            Length

            final JLabel length = new JLabel("Length Converter");
            length.setBounds(90,300,120,20);

            final TextField txt4 = new TextField();
            txt4.setBounds(10, 330, 260, 25);

            JButton bMetreToKilometre = new JButton("Metre to Kilometr");
            bMetreToKilometre.setBounds(10, 360, 130, 20);
            bMetreToKilometre.setFont(font);

            JButton bMetreToMile = new JButton("Metre to Mile");
            bMetreToMile.setBounds(10, 390, 130, 20);
            bMetreToMile.setFont(font);

            JButton bMetreToNauMile = new JButton("Metre to NauMile");
            bMetreToNauMile.setBounds(10, 420, 130, 20);
            bMetreToNauMile.setFont(font);

            JButton bMetreToCable = new JButton("Metre to Cable");
            bMetreToCable.setBounds(10, 450, 130, 20);
            bMetreToCable.setFont(font);

            JButton bMetreToLeague = new JButton("Metre to League");
            bMetreToLeague.setBounds(10, 480, 130, 20);
            bMetreToLeague.setFont(font);

            JButton bMetreToFoot = new JButton("Metre to Foot");
            bMetreToFoot.setBounds(10, 510, 130, 20);
            bMetreToFoot.setFont(font);

            JButton bMetreToYard = new JButton("Metre to Yard");
            bMetreToYard.setBounds(10, 540, 130, 20);
            bMetreToYard.setFont(font);

            JButton bKilometreToMetre = new JButton("Kilometr to Metre");
            bKilometreToMetre.setBounds(140, 360, 130, 20);
            bKilometreToMetre.setFont(font);

            JButton bMileToMetre = new JButton("Mile to Metre");
            bMileToMetre.setBounds(140, 390, 130, 20);
            bMileToMetre.setFont(font);

            JButton bNauMileToMetre = new JButton("NauMile to Metre");
            bNauMileToMetre.setBounds(140, 420, 130, 20);
            bNauMileToMetre.setFont(font);

            JButton bCableToMetre = new JButton("Cable to Metre");
            bCableToMetre.setBounds(140, 450, 130, 20);
            bCableToMetre.setFont(font);

            JButton bLeagueToMetre = new JButton("League to Metre");
            bLeagueToMetre.setBounds(140, 480, 130, 20);
            bLeagueToMetre.setFont(font);

            JButton bFootToMetre = new JButton("Foot to Metre");
            bFootToMetre.setBounds(140, 510, 130, 20);
            bFootToMetre.setFont(font);

            JButton bYardToMetre = new JButton("Yard to Metre");
            bYardToMetre.setBounds(140, 540, 130, 20);
            bYardToMetre.setFont(font);

            add(txt4);
            add(length);
            add(bMetreToKilometre);
            add(bMetreToMile);
            add(bMetreToNauMile);
            add(bMetreToCable);
            add(bMetreToLeague);
            add(bMetreToFoot);
            add(bMetreToYard);
            add(bKilometreToMetre);
            add(bMileToMetre);
            add(bNauMileToMetre);
            add(bCableToMetre);
            add(bLeagueToMetre);
            add(bFootToMetre);
            add(bYardToMetre);

            bMetreToKilometre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToKilometre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToMile.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToMile";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToNauMile.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToNauMile";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToCable.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToCable";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToLeague.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToLeague";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToFoot.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToFoot";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMetreToYard.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MetreToYard";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });


            bKilometreToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "KilometreToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bMileToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "MileToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bNauMileToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "NauMileToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bCableToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "CableToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bLeagueToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "LeagueToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bFootToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "FootToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

            bYardToMetre.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt4.getText());
                    op = "YardToMetre";
                    double num = Double.valueOf(txt4.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt4.setText(strRes);
                }
            });

//            Temperature

            final JLabel temperature = new JLabel("Temperature Converter");
            temperature.setBounds(370,300,135,20);

            final TextField txt5 = new TextField();
            txt5.setBounds(300, 330, 260, 25);

            JButton bCeToKe = new JButton("Cels to Kelvin");
            bCeToKe.setBounds(300, 360, 130, 20);
            bCeToKe.setFont(font);

            JButton bCetoFa = new JButton("Cels to Fahrenh");
            bCetoFa.setBounds(300, 390, 130, 20);
            bCetoFa.setFont(font);

            JButton bCeToRe = new JButton("Cels to Reaumur");
            bCeToRe.setBounds(300, 420, 130, 20);
            bCeToRe.setFont(font);

            JButton bCeToRo = new JButton("Cels to Roemer");
            bCeToRo.setBounds(300, 450, 130, 20);
            bCeToRo.setFont(font);

            JButton bCeToRa = new JButton("Cels to Rankin");
            bCeToRa.setBounds(300, 480, 130, 20);
            bCeToRa.setFont(font);

            JButton bCeToNy = new JButton("Cels to Newton");
            bCeToNy.setBounds(300, 510, 130, 20);
            bCeToNy.setFont(font);

            JButton bCeToDe = new JButton("Cels to Delisle");
            bCeToDe.setBounds(300, 540, 130, 20);
            bCeToDe.setFont(font);


            JButton bKeToCe = new JButton("Kelvin to Cels");
            bKeToCe.setBounds(430, 360, 130, 20);
            bKeToCe.setFont(font);

            JButton bFaToCe = new JButton("Fahrenh to Cels");
            bFaToCe.setBounds(430, 390, 130, 20);
            bFaToCe.setFont(font);

            JButton bReToCe = new JButton("Reaumur to Cels");
            bReToCe.setBounds(430, 420, 130, 20);
            bReToCe.setFont(font);

            JButton bRoToCe = new JButton("Roemer to Cels");
            bRoToCe.setBounds(430, 450, 130, 20);
            bRoToCe.setFont(font);

            JButton bRaToCe = new JButton("Rankin to Cels");
            bRaToCe.setBounds(430, 480, 130, 20);
            bRaToCe.setFont(font);

            JButton bNyToCe = new JButton("Newton to Cels");
            bNyToCe.setBounds(430, 510, 130, 20);
            bNyToCe.setFont(font);

            JButton bDeToCe = new JButton("Delisle to Cels");
            bDeToCe.setBounds(430, 540, 130, 20);
            bDeToCe.setFont(font);

            add(txt5);
            add(temperature);
            add(bCeToKe);
            add(bCetoFa);
            add(bCeToRe);
            add(bCeToRo);
            add(bCeToRa);
            add(bCeToNy);
            add(bCeToDe);
            add(bKeToCe);
            add(bFaToCe);
            add(bReToCe);
            add(bRoToCe);
            add(bRaToCe);
            add(bNyToCe);
            add(bDeToCe);

            bCeToKe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToKe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCetoFa.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CetoFa";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCeToRe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToRe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCeToRo.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToRo";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCeToRa.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToRa";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCeToNy.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToNy";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bCeToDe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "CeToDe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });


            bKeToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "KeToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bFaToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "FaToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bReToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "ReToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bRoToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "RoToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bRaToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "RaToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bNyToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "NyToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

            bDeToCe.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Double.valueOf(txt5.getText());
                    op = "DeToCe";
                    double num = Double.valueOf(txt5.getText());
                    String strOp = op;
                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp));
                    txt5.setText(strRes);
                }
            });

        }
        catch (ArithmeticException exception)
        {
            //System.out.println("Exceptionale erorrationale");
        }
    }

}